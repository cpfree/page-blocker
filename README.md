## 获取扩展

`Microsoft Edge` 扩展, 可以通过 <https://microsoftedge.microsoft.com/addons/detail/ecpndgdlfpabocbnhdpgkepbjbmegbjo> 获取扩展.

`Microsoft Edge` 扩展里面, 通过搜索 `滚去学习`, 或 `pageblocker` 可以搜索到该插件.

## 面板介绍

该插件主要是为了**屏蔽一些娱乐网站**, **当打开一些页面时, 插件将会将页面替换为`do or die`的内容(暂不支持定制页面).**.

> 自己和周边朋友都是使用 `chromium` 内核浏览器进行看视频娱乐, 而修改 `host` 文件等拦截方式, 在多台设备上面不方便.
> 
> 于是自己**简单**制作了这样一个插件. 给上网娱乐设置一点障碍, 提升些自律力.

---

该插件默认屏蔽了下面5个视频网站

   1. 爱奇艺
   2. 哔哩哔哩
   3. 腾讯视频
   4. 芒果TV
   5. YouTube

但是如果你打开下面的`拦截所有页面`的选项, 则会屏蔽掉所有的网站

<div class="option">
   <span class="name">拦截所有页面：</span>
   <label class="switch">
      <input type="checkbox" id="switch">
      <span class="slider round"></span>
   </label>
</div>

这时建议对该插件配置: 到`管理扩展`下配置`站点访问权限`为`在特定网站上`, 将需要阻止的网站添加到允许列表里面即可对这些网站页面进行`拦截替换`.

![](img/1641356077098.jpg)
